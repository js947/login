from alpine
run apk add --no-cache openssh

add https://github.com/js947.keys /root/.ssh/authorized_keys
run chmod 600 -R /root/.ssh

entrypoint ["/usr/sbin/sshd"]
cmd ["-D", "-e"]

expose 22/tcp
